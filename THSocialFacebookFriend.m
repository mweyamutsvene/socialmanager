//
//  THSocialFacebookFriend.m
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import "THSocialFacebookFriend.h"

NSString * const kFacebookFriendData =  @"data";
NSString * const kFacebookFriendId =  @"id";
NSString * const kFacebookFriendGender =  @"gender";
NSString * const kFacebookFriendLastName =  @"last_name";
NSString * const kFacebookFriendFirstName =  @"first_name";
NSString * const kFacebookFriendFullName =  @"name";
NSString * const kFacebookFriendLink=  @"link";
NSString * const kFacebookFriendPicture =  @"picture";
NSString * const kFacebookFriendPictureData =  @"data";
NSString * const kFacebookFriendPictureSilhouette =  @"is_silhouette";
NSString * const kFacebookFriendPictureURL=  @"url";
NSString * const kFacebookFriendUserName =  @"username";


#pragma mark - Private Interface
@interface THSocialFacebookFriend ()
@end

@implementation THSocialFacebookFriend
#pragma mark - Constructors
+(NSArray*)friendsFromDictionary:(NSDictionary*)friendDictionary;
{
    NSMutableArray* friends = [NSMutableArray array];
    
    //Get all friends in response dictionary
    for (NSDictionary* friendDict in [friendDictionary objectForKey:kFacebookFriendData]) {
        THSocialFacebookFriend* friend = [[THSocialFacebookFriend alloc] initWithDictionary:friendDict];
        [friends addObject:friend];
    }
    return friends;
}

//
//Create friend object based on data returned from Graph API
-(id)initWithDictionary:(NSDictionary*)dictionary;
{
  self = [super init];
  if(self == nil)
    return nil;
    //Fill out information
    self.name = [dictionary objectForKey:kFacebookFriendFullName];
    self.facebookId = [dictionary objectForKey:kFacebookFriendId];
    self.facebookAvatarImage = [[[dictionary objectForKey:kFacebookFriendPicture]
                                 objectForKey:kFacebookFriendPictureData]
                                objectForKey:kFacebookFriendPictureURL];
    self.originalResponse = dictionary;
    //TODO: Fill out more detailed information, for right now this is all I need
  return self;
}

-(void)dealloc{
}
#pragma mark - Accessors

#pragma mark - Private Methods
#pragma mark - Public Methods
@end

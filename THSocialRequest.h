//
//  THSocialRequest.h
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "THSocialFacebookFriend.h"

//Enumeration for the request Method Type
typedef enum
{
    SOCIAL_FACEBOOK_FRIENDLIST,
    SOCIAL_FACEBOOK_MY_PROFILE,
    SOCIAL_FACEBOOK_COMPOSE,
    SOCIAL_FACEBOOK_FEED,
    
    SOCIAL_TWITTER_DATA,
    SOCIAL_TWITTER_FEED,
    SOCIAL_TWITTER_COMPOSE
    
} THSocialRequestType;

@class THSocialRequest;
@protocol THSocialRequestDelegate <NSObject>
@required
-(void)request:(THSocialRequest*)request wasSuccessful:(BOOL)success withResponse:(id)response;
@end

@interface THSocialRequest : NSObject
@property (assign) NSObject<THSocialRequestDelegate>* delegate;
@property (assign) THSocialRequestType requestType;
@property (strong) id persistedObject;
@property (strong) NSDictionary* params;
@property (strong) NSURL* path;
@property (strong) NSString* accountType;
@property (assign) int method;
//Handle a successful response from the server
-(void)handleResponse:(id)responseObject;

//Handle error/failure from server
-(void)handleFailure:(NSError*)error;

//Cancels the current request
-(void)cancelRequest;

//Get Requests
//Open Graph API
+(THSocialRequest*)getMyInfoWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
+(THSocialRequest*)getFriendListWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
+(THSocialRequest*)getMoreListWithURL:(NSURL*)url andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
+(THSocialRequest*)postMessageToFeed:(NSString*)message link:(NSString*)link description:(NSString*)description actionLinks:(NSDictionary*)actions andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;

//Twitter API
+(THSocialRequest*)postMessageToTwitterFeed:(NSString*)message link:(NSString*)link andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
+(THSocialRequest*)getUserDataWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;

//Compose controller
+(THSocialRequest*)facebookComposeWithURL:(NSURL*)url image:(UIImage*)image prefilledText:(NSString*)text andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
+(THSocialRequest*)twitterComposeWithURL:(NSURL*)url image:(UIImage*)image prefilledText:(NSString*)text andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;


@end

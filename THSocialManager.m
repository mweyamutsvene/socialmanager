//
//  CTSocialManager.m
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import "THSocialManager.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "THSocialRequest.h"
NSString * const kADBundleInfoFacebookAppID =  @"FacebookAppID";

#pragma mark - Private Interface
@interface THSocialManager ()
@end

@implementation THSocialManager
#pragma mark - Constructors
-(id)init{
    self = [super init];
    if(self == nil)
        return nil;
    
    return self;
}

-(void)dealloc{
}
#pragma mark - Accessors
+ (UIViewController *)topViewController
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    return [self topViewController:rootViewController];
}

+ (UIViewController *)topViewController:(UIViewController *)viewController{
    
    if (viewController.presentedViewController){
        UIViewController *presentedViewController = viewController.presentedViewController;
        return [self topViewController:presentedViewController];
    }
    else if ([viewController isKindOfClass:[UITabBarController class]]){
        UITabBarController *tabBarController = (UITabBarController *)viewController;
        return [self topViewController:tabBarController.selectedViewController];
    }
    
    else if ([viewController isKindOfClass:[UINavigationController class]]){
        UINavigationController *navController = (UINavigationController *)viewController;
        return [self topViewController:navController.visibleViewController];
    }
    
    // Handling UIViewController's added as subviews to some other views.
    else {
        NSInteger subCount = [viewController.view subviews].count - 1;
        
        for (NSInteger index = subCount; index >=0 ; --index){
            UIView *view = [[viewController.view subviews] objectAtIndex:index];
            id subViewController = [view nextResponder];    // Key property which most of us are unaware of / rarely use.
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]]){
                return [self topViewController:subViewController];
            }
        }
        return viewController;
    }
}


-(NSString*)accessToken;
{
    //we get our account info
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSArray *accounts = [accountStore accountsWithAccountType:facebookAccountType];
    ACAccount *facebookAccount = [accounts lastObject];
    
    ACAccountCredential *fbCredential = [facebookAccount credential];
    NSString *accessToken = [fbCredential oauthToken];
    return accessToken;
}


SYNTHESIZE_SINGLETON_FOR_CLASS(THSocialManager)

//Check if facebook is available
-(BOOL)facebookIsAvailable
{
    if([SLComposeViewController class]){
        return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
    }
    
    //Sanity check, in ios6+ we should never get here
    return false;
}

//Check if facebook is available
-(BOOL)twitterIsAvailable
{
    if([SLComposeViewController class]){
        return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
    }
    
    //Sanity check, in ios6+ we should never get here
    return false;
}

#pragma mark - Private Methods
//
//Handle response failure
-(void)handleFacebookFailedPermissionResponseWithError:(NSError*)e forRequest:(THSocialRequest*)request;
{
    if(e == nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [request handleFailure:[NSError errorWithDomain:@"com.ifoundry" code:403 userInfo:nil]];
        });
    }
    else if(e.code == 7){
        dispatch_async(dispatch_get_main_queue(), ^{
            [request handleFailure:e];
            //[self askForReadPermissions];
            
        });
    }
    else{
        // Handle Failure
        dispatch_async(dispatch_get_main_queue(), ^{
            [request handleFailure:e];
        });
        
        
    }
    
}
//
//Ask the account store for access to the facebook account/ write permissions
-(void)askForFacebookReadPermissionsAndPerformBlockOnCompletion:(void (^)())completion forRequest:(THSocialRequest*)request{
    NSLog(@"Asking for Facebook Read Permissions");
    //Sanity check to make sure we are in the correct os
    if([SLComposeViewController class]){
        //we get our account info
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        NSString* facebookAppID = [[[NSBundle mainBundle] infoDictionary] objectForKey:kADBundleInfoFacebookAppID];
        
        // Specify App ID and permissions
        NSDictionary *options;
        if(facebookAppID){
            options= @{
                       ACFacebookAppIdKey: facebookAppID,
                       ACFacebookPermissionsKey: @[@"email"]
                       };
            
        }
        else{
            options= @{
                       ACFacebookPermissionsKey: @[@"user_location", @"email"]
                       };
        }
        //Request access to account
        
        [accountStore requestAccessToAccountsWithType:facebookAccountType
                                              options:options
                                           completion:^(BOOL granted, NSError *e){
                                               if(granted){
                                                   NSLog(@"Read Permissions Granted");
                                                   //We have been granted access, perform the original operation
                                                   completion();
                                               }
                                               else{
                                                   NSLog(@"Read Permissions Failed");
                                                   //We have not been granted access - respond to failure
                                                   [self handleFacebookFailedPermissionResponseWithError:e
                                                                                              forRequest:request];
                                               }
                                           }];
    }
}
//
//Ask the account store for access to the facebook account / write permissions
-(void)askForFacebookWritePermissionsAndPerformBlockOnCompletion:(void (^)())completion forRequest:(THSocialRequest*)request{
    NSLog(@"Asking for Facebook Write Permissions");
    //Sanity check to make sure we are in the correct os
    if([SLComposeViewController class]){
        //we get our account info
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        NSString* facebookAppID = [[[NSBundle mainBundle] infoDictionary] objectForKey:kADBundleInfoFacebookAppID];
        
        // Specify App ID and permissions
        NSDictionary *options = @{
                                  ACFacebookAppIdKey: facebookAppID,
                                  ACFacebookPermissionsKey: @[@"publish_actions"],
                                  ACFacebookAudienceKey: ACFacebookAudienceFriends
                                  };
        //Request access to account
        [accountStore requestAccessToAccountsWithType:facebookAccountType
                                              options:options
                                           completion:^(BOOL granted, NSError *e){
                                               if(granted){
                                                   NSLog(@"Write Permissions Granted");
                                                   //We have been granted access, perform the original operation
                                                   completion();
                                               }
                                               else{
                                                   NSLog(@"Write Permissions Failed");
                                                   //We have not been granted access - respond to failure
                                                   [self handleFacebookFailedPermissionResponseWithError:e
                                                                                              forRequest:request];
                                               }
                                           }];
    }
}
//
//Ask the account store for access to the facebook account
-(void)askForTwitterReadPermissionsAndPerformBlockOnCompletion:(void (^)())completion forRequest:(THSocialRequest*)request{
    NSLog(@"Asking for Twitter Read Permissions");
    //Sanity check to make sure we are in the correct os
    if([SLComposeViewController class]){
        //we get our account info
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:request.accountType];
        
        //Request access to account
        [accountStore requestAccessToAccountsWithType:accountType
                                              options:nil
                                           completion:^(BOOL granted, NSError *e){
                                               if(granted){
                                                   NSLog(@"Read Permissions Granted");
                                                   //We have been granted access, perform the original operation
                                                   completion();
                                               }
                                               else{
                                                   NSLog(@"Read Permissions Failed");
                                                   //We have not been granted access - respond to failure
                                                   [self handleFacebookFailedPermissionResponseWithError:e
                                                                                              forRequest:request];
                                               }
                                           }];
    }
}
#pragma mark - Public Methods
-(void)performCompose:(THSocialRequest*)request;
{
    SLComposeViewController *composer;
    if(request.requestType == SOCIAL_FACEBOOK_COMPOSE){
        composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    }
    else if (request.requestType == SOCIAL_TWITTER_COMPOSE){
        composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    }
    
    //Prefill the text
    if(request.params[@"text"]){
        [composer setInitialText:request.params[@"text"]];
    }
    
    if(request.params[@"image"]){
        [composer addImage:request.params[@"image"]];
    }
    //the Link you want to post
    [composer addURL:request.path];
    
    [[THSocialManager topViewController] presentViewController:composer animated:true completion:nil];

    [composer setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Facebook Result: Cancelled");
                [request handleFailure:nil];
                break;
            case SLComposeViewControllerResultDone:{
                NSLog(@"Facebook Result: Sent");
                [request handleResponse:nil];
                break;
            }
            default:
                break;
        }
    }];
    
    
    
}
//
//Perform Request with paramater from request
-(void)performRequest:(THSocialRequest*)request;
{
    //we get our account info
    BOOL isFacebook = [request.accountType isEqualToString:ACAccountTypeIdentifierFacebook];
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:request.accountType];
    
    NSString* serviceType= (isFacebook) ? SLServiceTypeFacebook : SLServiceTypeTwitter;
    SLRequestMethod method = request.method;
    
    void(^writeCompletionBlock)(void) = ^()
    {
        NSArray *accounts = [accountStore accountsWithAccountType:accountType];
        ACAccount *account = [accounts lastObject];
        NSDictionary *param= request.params;
        NSURL *feedURL = request.path;
        
        if(request.requestType == SOCIAL_TWITTER_DATA){
            param = [NSDictionary dictionaryWithObjectsAndKeys:account.username,@"screen_name",nil];
            
        }
        //Create the feed request
        SLRequest *feedRequest = [SLRequest
                                  requestForServiceType:serviceType
                                  requestMethod:method
                                  URL:feedURL
                                  parameters:param];
        
        feedRequest.account = account;
        [feedRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             if(!error)
             {
                 //Parse the response
                 id json =[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
                 
                 NSLog(@"Dictionary contains data: %@", json );
                 if([json objectForKey:@"error"]!=nil)
                 {
                     //Dont know why this use case isn't handled in the error... stupid facebook
                     [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                         NSLog(@"%@",error);
                         
                         //Check renewal
                         if(renewResult == ACAccountCredentialRenewResultRenewed){
                             //try again
                             [self performRequest:request];
                             return;
                         }
                         else{
                             //Handle this error or atleast inform request of failure
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [request handleFailure:error];
                             });
                             return;
                             
                         }
                     }];
                     return;
                 }
                 dispatch_async(dispatch_get_main_queue(),^{
                     //Request has been completed
                     [request handleResponse:json];
                 });
             }
             else{
                 //handle error gracefully
                 NSLog(@"error from get%@",error);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [request handleFailure:error];
                 });
                 
                 //attempt to revalidate credentials or something
             }
         }];
        
    };
    
    void(^readCompletionBlock)(void) = ^()
    {
        [self askForFacebookWritePermissionsAndPerformBlockOnCompletion:writeCompletionBlock forRequest:request];
    };
    
    //Ask account for read permissions
    if(isFacebook){
        [self askForFacebookReadPermissionsAndPerformBlockOnCompletion:readCompletionBlock forRequest:request];
    }
    else{
        method = SLRequestMethodPOST;
        [self askForTwitterReadPermissionsAndPerformBlockOnCompletion:writeCompletionBlock forRequest:request];
    }
}



@end

//
//  THSocialRequest.m
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import "THSocialRequest.h"
#import "THSocialManager.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

NSString * const kFacebookFriendListPagination =  @"paging";
NSString * const kFacebookFriendListPaginationURL =  @"next";

#pragma mark - Private Interface
@interface THSocialRequest ()
@end

@implementation THSocialRequest
#pragma mark - Constructors
-(id)init{
    self = [super init];
    if(self == nil)
        return nil;
    
    return self;
}

-(void)dealloc{
}
#pragma mark - Accessors

#pragma mark - Private Methods

//
//Handle a successful response from the server
-(void)handleResponse:(id)responseObject;
{
    switch (self.requestType) {
        case SOCIAL_FACEBOOK_FRIENDLIST:{
            //convert response into friend object
            NSArray* friends = [THSocialFacebookFriend friendsFromDictionary:responseObject];
            
            //Save Pagination URL for later Use
            NSString* urlString = [[responseObject objectForKey:kFacebookFriendListPagination] objectForKey:kFacebookFriendListPaginationURL];
            NSURL* url = [NSURL URLWithString:urlString];
            self.persistedObject = url;
            
            //Pass on the savings
            [_delegate request:self wasSuccessful:true withResponse:friends];
            
            break;
        }
        case SOCIAL_FACEBOOK_MY_PROFILE:{
            
            //Get My Profile information
            THSocialFacebookFriend* myProfile = [[THSocialFacebookFriend alloc] initWithDictionary:responseObject];
            [_delegate request:self wasSuccessful:true withResponse:myProfile];
            
            break;
        }
        default:
            [_delegate request:self wasSuccessful:true withResponse:responseObject];
            
            break;
    }
    
}

//Handle error/failure from server
-(void)handleFailure:(NSError*)error;
{
    NSLog(@"Request Failed: %@",error.description);
    NSString* helpfulErrorMessage = nil;
    switch (error.code) {
        case 6:{
            //User has not set up a facebook account
            helpfulErrorMessage =NSLocalizedString(@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings.", @"No Facebook Account Detected Error");
            break;
        }
        case 7:{
            //User has denied permission to use facebook
            helpfulErrorMessage =NSLocalizedString(@"You have previously denied access from this app, please go to your device settings and grant access", @"User Denied Access Error");
            break;
        }
        default:
            //Something else happend
            helpfulErrorMessage = NSLocalizedString(@"Unable to connect to facebook, please try again later", @"Un Handled Facebook Error");
            break;
    }
    
    //Inform the delegate
    [_delegate request:self wasSuccessful:false withResponse:helpfulErrorMessage];
    
}

//Cancels the current request
-(void)cancelRequest;
{
    //With this type of request, we cant really cancel - so we'll just ignore the response
    self.delegate = nil;
}

#pragma mark - Public Methods
+(THSocialRequest*)getMyInfoWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.accountType = ACAccountTypeIdentifierFacebook;
    request.requestType = SOCIAL_FACEBOOK_MY_PROFILE;
    request.delegate = delegate;
    request.path = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:@"picture,id,name,link,gender,last_name,first_name,username",@"fields", nil];
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performRequest:request];
    return request;
}

+(THSocialRequest*)getFriendListWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    return [THSocialRequest getMoreListWithURL:[NSURL URLWithString:@"https://graph.facebook.com/me/friends"] andDelegate:delegate];
}

+(THSocialRequest*)getMoreListWithURL:(NSURL*)url andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.requestType = SOCIAL_FACEBOOK_FRIENDLIST;
    request.delegate = delegate;
    request.params = [NSDictionary dictionaryWithObjectsAndKeys:@"picture,id,name,link,gender,last_name,first_name,username",@"fields", nil];
    request.path = url;
    if(!url){
        request.path = [NSURL URLWithString:@"https://graph.facebook.com/me/friends"];
    }
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performRequest:request];
    return request;
    
}

+(THSocialRequest*)postMessageToFeed:(NSString*)message link:(NSString*)link description:(NSString*)description actionLinks:(NSDictionary*)actions andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:actions
    //                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
    //                                                         error:nil];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //TODO: do something with actionlinks
    NSDictionary *postDict = @{@"link": link,
                               @"message" : message,
                               @"description" : description};
    
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.accountType = ACAccountTypeIdentifierFacebook;
    request.requestType = SOCIAL_FACEBOOK_FEED;
    request.delegate = delegate;
    request.params = postDict;
    request.path = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
    request.method = SLRequestMethodPOST;
    
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performRequest:request];
    return request;
    
}


+(THSocialRequest*)postMessageToTwitterFeed:(NSString*)message link:(NSString*)link andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:actions
    //                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
    //                                                         error:nil];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //TODO: do something with actionlinks
    NSDictionary *postDict = @{@"status" : message};
    
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.accountType = ACAccountTypeIdentifierTwitter;
    request.requestType = SOCIAL_TWITTER_FEED;
    request.delegate = delegate;
    request.params = postDict;
    request.path = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    request.method = SLRequestMethodPOST;
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performRequest:request];
    return request;
    
}

+(THSocialRequest*)getUserDataWithDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:actions
    //                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
    //                                                         error:nil];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //TODO: do something with actionlinks
    //    NSDictionary *postDict = @{@"status" : message};
    
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.accountType = ACAccountTypeIdentifierTwitter;
    request.requestType = SOCIAL_TWITTER_DATA;
    request.delegate = delegate;
    request.params = nil;
    request.path = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/show.json"];
    request.method = SLRequestMethodGET;
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performRequest:request];
    return request;
    
}


#pragma mark - Composer Methods
+(THSocialRequest*)facebookComposeWithURL:(NSURL*)url image:(UIImage*)image prefilledText:(NSString*)text andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.requestType = SOCIAL_FACEBOOK_COMPOSE;
    request.delegate = delegate;
    request.path = url;
    
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    
    if(text){
        [params setObject:text forKey:@"text"];
    }
    
    if(image){
        [params setObject:image forKey:@"image"];
    }
    request.params = params;
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performCompose:request];
    return request;
    
}

+(THSocialRequest*)twitterComposeWithURL:(NSURL*)url image:(UIImage*)image prefilledText:(NSString*)text andDelegate:(NSObject<THSocialRequestDelegate>*)delegate;
{
    THSocialRequest* request = [[THSocialRequest alloc] init];
    request.requestType = SOCIAL_TWITTER_COMPOSE;
    request.delegate = delegate;
    request.path = url;
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    
    if(text){
        [params setObject:text forKey:@"text"];
    }
    
    if(image){
        [params setObject:image forKey:@"image"];
    }
    request.params = params;
    
    // Perform Request
    [[THSocialManager sharedTHSocialManager] performCompose:request];
    return request;
}

@end
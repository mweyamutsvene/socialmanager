//
//  THSocialFacebookFriend.h
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
@class THSocialFacebookFriend;
@protocol THSocialFacebookFriendDelegate <NSObject>
@end

@interface THSocialFacebookFriend : NSObject
@property (assign) NSObject<THSocialFacebookFriendDelegate>* delegate;
@property (strong) NSString* facebookId;
@property (strong) NSString* name;
@property (strong) NSString* facebookAvatarImage;
@property (strong) NSDictionary* originalResponse;

//Get friends from given |NSDictionary| returned by facebook API
+(NSArray*)friendsFromDictionary:(NSDictionary*)friendDictionary;

//Create friend object based on data returned from Graph API
-(id)initWithDictionary:(NSDictionary*)dictionary;@end

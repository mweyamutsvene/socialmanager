//
//  CTSocialManager.h
//  THTicTacToe
//
//  Created by Thomas Hanks on 11/9/13.
//  Copyright (c) 2013 Tommy Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"
@class THSocialManager, THSocialRequest;
@protocol THSocialManagerDelegate <NSObject>
@end

@interface THSocialManager : NSObject
@property (assign) NSObject<THSocialManagerDelegate>* delegate;
SYNTHESIZE_SINGLETON_METHOD_FOR_CLASS(THSocialManager)
-(NSString*)accessToken;
-(BOOL)facebookIsAvailable;
-(BOOL)twitterIsAvailable;

//Uses facebook graph api
-(void)performRequest:(THSocialRequest*)request;

//Uses the native composer to display post
-(void)performCompose:(THSocialRequest*)request;

@end
